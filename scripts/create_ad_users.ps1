﻿#
# PowerShell Script written by Raphael Perez - raphael@rflsystems.co.uk
# Adapted by Baptiste MOINE - contact@bmoine.fr
#

Import-Module ActiveDirectory

# Random password function.
function New-SWRandompassword {
    [CmdletBinding(ConfirmImpact='Low')]
    [OutputType([String])]
    Param
    (
        # Specifies minimum password length.
        [Parameter(Mandatory=$false, 
                   ValueFromPipeline=$false,
                   ValueFromPipelineByPropertyName=$true, 
                   ValueFromRemainingArguments=$false, 
                   Position=0)]
        [ValidateScript({$_ -gt 0})]
        [Alias("Min")] 
        [int]$MinpasswordLength = 20,
        
        # Specifies maximum password length.
        [Parameter(Mandatory=$false, 
                   ValueFromPipeline=$false,
                   ValueFromPipelineByPropertyName=$true, 
                   ValueFromRemainingArguments=$false, 
                   Position=1)]
        [ValidateScript({$_ -ge $MinpasswordLength})]
        [Alias("Max")]
        [int]$MaxpasswordLength = 20,
        
        # Specifies an array of strings containing charactergroups from which the password will be generated.
        # At least one char from each group (string) will be used.
        [Parameter(Mandatory=$false, 
                   ValueFromPipeline=$false,
                   ValueFromPipelineByPropertyName=$true, 
                   ValueFromRemainingArguments=$false, 
                   Position=2)]
        [String[]]$InputStrings = @('abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', '0123456789', '!"#$%&\()*+,-./:<=>?@[\\]^_`{|}~'),
        
        # Specifies number of passwords to generate.
        [Parameter(Mandatory=$false, 
                   ValueFromPipeline=$false,
                   ValueFromPipelineByPropertyName=$true, 
                   ValueFromRemainingArguments=$false, 
                   Position=3)]
        [ValidateScript({$_ -gt 0})]
        [int]$Count = 1
    )
    Begin {
        Function Get-Seed{
            # Generate a seed for future randomization.
            $RandomBytes = New-Object -TypeName 'System.Byte[]' 4
            $Random = New-Object -TypeName 'System.Security.Cryptography.RNGCryptoServiceProvider'
            $Random.GetBytes($RandomBytes)
            [BitConverter]::ToInt32($RandomBytes, 0)
        }
    }
    Process {
        For($iteration = 1;$iteration -le $Count; $iteration++){
            # Create char arrays containing possible chars.
            [char[][]]$Chargroups = $InputStrings

            # Set counter of used groups.
            [int[]]$Usedgroups = for($i=0;$i -lt $Chargroups.Count;$i++){0}

            # Create new char-array to hold generated password.
            if($MinpasswordLength -eq $MaxpasswordLength) {
                # If password length is set, use it.
                $password = New-Object -TypeName 'System.Char[]' $MinpasswordLength
            }
            else {
                # Otherwise randomize password length.
                $password = New-Object -TypeName 'System.Char[]' (Get-Random -SetSeed $(Get-Seed) -Minimum $MinpasswordLength -Maximum $($MaxpasswordLength+1))
            }

            for($i=0;$i -lt $password.Length;$i++){
                if($i -ge ($password.Length - ($Usedgroups | Where-Object {$_ -eq 0}).Count)) {
                    # Check if number of unused groups are equal of less than remaining chars.
                    # Select first unused Chargroup.
                    $ChargroupIndex = 0
                    while(($Usedgroups[$ChargroupIndex] -ne 0) -and ($ChargroupIndex -lt $Chargroups.Length)) {
                        $ChargroupIndex++
                    }
                }
                else {
                    # Select Random group.
                    $ChargroupIndex = Get-Random -SetSeed $(Get-Seed) -Minimum 0 -Maximum $Chargroups.Length
                }

                # Set current position in password to random char from selected group using a random seed.
                $password[$i] = Get-Random -SetSeed $(Get-Seed) -InputObject $Chargroups[$ChargroupIndex]
                # Update count of used groups.
                $Usedgroups[$ChargroupIndex] = $Usedgroups[$ChargroupIndex] + 1
            }
            Write-Output -InputObject $($password -join '')
        }
    }
}

$users = Import-Csv -Delimiter ";" -Encoding UTF8 -Path ".\userlist.csv"

foreach ($user in $users) {  
    $ou = $user.adspath

    $firstname = $user.firstname
    $lastname = $user.lastname
    $comp_name = $firstname + " " + $lastname

    # Login example: moinebap
    if ($lastname.length -ge 5) {
        $FirstLetterlastname = $lastname.substring(0,5)
    } else {
        $FirstLetterlastname = $lastname.substring(0,$lastname.length)
    }

    if ($firstname.length -ge 3) {
        $FirstLetterfirstname = $firstname.substring(0,3)
    } else {
        $FirstLetterfirstname = $firstname.substring(0,$firstname.length)
    }

    $login = ($FirstLetterlastname + $FirstLetterfirstname).ToLower()

    # Fix charset.
    $login = $login.Replace("à", "a")
    $login = $login.Replace("á", "a")
    $login = $login.Replace("â", "a")
    $login = $login.Replace("ã", "a")
    $login = $login.Replace("ä", "a")
    $login = $login.Replace("å", "a")
    $login = $login.Replace("ā", "a")
    $login = $login.Replace("ă", "a")
    $login = $login.Replace("ą", "a")
    $login = $login.Replace("æ", "a")

    $login = $login.Replace("ç", "c")
    $login = $login.Replace("ć", "c")
    $login = $login.Replace("ĉ", "c")
    $login = $login.Replace("ċ", "c")
    $login = $login.Replace("č", "c")

    $login = $login.Replace("ď", "d")
    $login = $login.Replace("đ", "d")

    $login = $login.Replace("è", "e")
    $login = $login.Replace("é", "e")
    $login = $login.Replace("ê", "e")
    $login = $login.Replace("ë", "e")
    $login = $login.Replace("ē", "e")
    $login = $login.Replace("ĕ", "e")
    $login = $login.Replace("ė", "e")
    $login = $login.Replace("ę", "e")
    $login = $login.Replace("ě", "e")

    $login = $login.Replace("ĝ", "g")
    $login = $login.Replace("ğ", "g")
    $login = $login.Replace("ġ", "g")
    $login = $login.Replace("ģ", "g")

    $login = $login.Replace("ħ", "h")
    $login = $login.Replace("ĥ", "h")

    $login = $login.Replace("ì", "i")
    $login = $login.Replace("í", "i")
    $login = $login.Replace("î", "i")
    $login = $login.Replace("ï", "i")
    $login = $login.Replace("ĩ", "i")
    $login = $login.Replace("ī", "i")
    $login = $login.Replace("ĭ", "i")
    $login = $login.Replace("į", "i")
    $login = $login.Replace("ı", "i")

    $login = $login.Replace("ĵ", "j")

    $login = $login.Replace("ķ", "k")
    $login = $login.Replace("ĸ", "k")

    $login = $login.Replace("ĺ", "l")
    $login = $login.Replace("ļ", "l")
    $login = $login.Replace("ľ", "l")
    $login = $login.Replace("ŀ", "l")
    $login = $login.Replace("ł", "l")

    $login = $login.Replace("ń", "n")
    $login = $login.Replace("ņ", "n")
    $login = $login.Replace("ň", "n")
    $login = $login.Replace("ŉ", "n")
    $login = $login.Replace("ñ", "n")

    $login = $login.Replace("ð", "o")
    $login = $login.Replace("ò", "o")
    $login = $login.Replace("ó", "o")
    $login = $login.Replace("ô", "o")
    $login = $login.Replace("õ", "o")
    $login = $login.Replace("ö", "o")
    $login = $login.Replace("ō", "o")
    $login = $login.Replace("ŏ", "o")
    $login = $login.Replace("ő", "o")
    $login = $login.Replace("œ", "oe")

    $login = $login.Replace("ŕ", "r")
    $login = $login.Replace("ř", "r")
    $login = $login.Replace("ŗ", "r")

    $login = $login.Replace("ś", "s")
    $login = $login.Replace("ŝ", "s")
    $login = $login.Replace("ş", "s")
    $login = $login.Replace("š", "s")

    $login = $login.Replace("ţ", "t")
    $login = $login.Replace("ŧ", "t")
    $login = $login.Replace("ť", "t")

    $login = $login.Replace("ù", "u")
    $login = $login.Replace("ú", "u")
    $login = $login.Replace("û", "u")
    $login = $login.Replace("ü", "u")
    $login = $login.Replace("ũ", "u")
    $login = $login.Replace("ū", "u")
    $login = $login.Replace("ŭ", "u")
    $login = $login.Replace("ů", "u")
    $login = $login.Replace("ű", "u")
    $login = $login.Replace("ų", "u")

    $login = $login.Replace("ŵ", "w")

    $login = $login.Replace("ý", "y")
    $login = $login.Replace("ŷ", "y")
    $login = $login.Replace("ÿ", "y")

    $login = $login.Replace("ź", "z")
    $login = $login.Replace("ż", "z")
    $login = $login.Replace("ž", "z")

    # Set password using CSV file.
    $password = $user.password
    
    # If password is empty, randomize newer with minimum of 20 chars.
    if ($password.Length -eq 0) { 
        $password = New-SWRandompassword 
        $user.password = $password
    }

    # Export or add new information to the CSV using utf8 encoding and ';' delimiters.
    $users | Export-Csv -Delimiter ';' -Path .\userlist.csv -NoTypeInformation -Encoding UTF8;
    (Get-Content -Path .\userlist.csv -Encoding Unicode -Raw) -replace '"','' | Set-Content -Encoding UTF8 -Path .\userlist.csv;
    
    # Finally create the user.
    New-ADUser -Name $comp_name -SamAccountName $login -UserPrincipalName $login -DisplayName $comp_name -GivenName $firstname -Surname $lastname -Accountpassword (ConvertTo-SecureString $password -AsPlainText -Force) -Enabled $true -Path $ou -KerberosEncryptionType "AES128, AES256"
}

Write-Host("Users should have been created, please remember to securely remove the userlist.csv file after printing (if applicable).")
